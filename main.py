#!/usr/bin/python3

import sys
from PyQt5.QtWidgets import QApplication
from GUI.MainWindow import MainWindow



app = QApplication(sys.argv)
debug = 'debug' in sys.argv
port = 1080
for arg in sys.argv:
	if 'port' in arg.lower() and '=' in arg.lower():
		port = int(arg.split("=")[1])
		print("port is: " + str(port))

mw = MainWindow(port)
if (debug):
	mw.show()  # fullscreen On release
else:
	mw.showFullScreen()  # windowed On debug


sys.exit(app.exec())
