from PyQt5.QtCore import QUrl
from PyQt5.QtWebEngineWidgets import QWebEngineView


class WebView(QWebEngineView):
	def __init__(self, parent, port):
		QWebEngineView.__init__(self, parent)
		self.address = "http://localhost:" + str(port)
		#if debug:
		#	self.address = "https://www.pracedru.dk:8889"

		self.load(QUrl(self.address))