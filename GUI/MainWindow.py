from PyQt5.QtCore import QEvent, Qt, QUrl, QMargins
from PyQt5.QtWidgets import QWidget, QVBoxLayout

from GUI.WebView import WebView

from subprocess import Popen, PIPE


class MainWindow(QWidget):
	def __init__(self, port):
		QWidget.__init__(self, None)
		self.host_process = Popen(["npm", "start"])
		self.port = port
		self.setMinimumHeight(720)
		self.setMinimumWidth(360)
		self.setMaximumHeight(720)
		self.setMaximumWidth(360)
		self.web_view = WebView(self, port)

		layout = QVBoxLayout()
		self.setLayout(layout)
		layout.setContentsMargins(QMargins(0, 0, 0, 0))
		layout.addWidget(self.web_view)
		self.installEventFilter(self)
		self.closeNow = False

	def eventFilter(self, obj, event):
		if event.type() == QEvent.KeyRelease:
			if event.key() == Qt.Key_Escape:
				print("closing")
				self.close()
				return True
			if event.key() == Qt.Key_F5:
				print("refresh")
				self.web_view.load(QUrl(self.web_view.address))
				return True
		return False

	def closeEvent(self, event):
		self.host_process.send_signal(19)
		self.host_process = None
		event.accept()

